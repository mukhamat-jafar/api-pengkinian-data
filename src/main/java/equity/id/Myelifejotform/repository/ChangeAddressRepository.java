package equity.id.Myelifejotform.repository;

import equity.id.Myelifejotform.model.ChangeAddress;
import equity.id.Myelifejotform.model.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ChangeAddressRepository extends JpaRepository<ChangeAddress, Long> {
}
