package equity.id.Myelifejotform.repository;

import equity.id.Myelifejotform.model.OTPRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface OTPRequestRepository extends JpaRepository<OTPRequest, Long> {
    @Query("select ro from OTPRequest ro where ro.otpCode = ?1")
    OTPRequest findByOtpCode(String otp);
}
