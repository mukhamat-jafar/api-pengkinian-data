package equity.id.Myelifejotform.dto.OTPRequest;

import lombok.Data;

import java.util.Date;

@Data
public class OTPRequestDto {
    private Long id;
    private String customerCode;
    private String customerName;
    private String phoneNo;
    private String email;
    private String genderCode;
    private String otpType;
    private String typeCode;
    private String otpCode;
    private String sentDate;
    private Date expiredTime;
    private boolean status;
}
