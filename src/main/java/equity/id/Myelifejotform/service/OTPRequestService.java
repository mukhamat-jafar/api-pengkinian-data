package equity.id.Myelifejotform.service;

import equity.id.Myelifejotform.dto.OTPRequest.OTPRequestDto;
import equity.id.Myelifejotform.model.OTPRequest;
import equity.id.Myelifejotform.repository.OTPRequestRepository;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;

@Service
public class OTPRequestService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private OTPRequestRepository otpRequestRepository;

    @Autowired
    private ModelMapper modelMapper;

    public String generateOTP(OTPRequest otpRequest, String getOTP){
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDateTime localDateNow = LocalDateTime.now();

        //Generate OTP 6 digit
        var otpCode = new DecimalFormat("000000").format(new Random().nextInt(999999));

        OTPRequest otpSaved = new OTPRequest();
        otpSaved.setCustomerCode(otpRequest.getCustomerCode());
        otpSaved.setCustomerName(otpRequest.getCustomerName());
        otpSaved.setExpiredTime(otpRequest.getExpiredTime());
        otpSaved.setGenderCode(otpRequest.getGenderCode());
        otpSaved.setOtpType(otpRequest.getOtpType());
        otpSaved.setPhoneNo(otpRequest.getPhoneNo());
        otpSaved.setEmail(otpRequest.getEmail());
        otpSaved.setTypeCode(otpRequest.getTypeCode());
        otpSaved.setSentDate(dtf.format(localDateNow));
        otpSaved.setOtpCode(otpCode);
        otpSaved.setStatus(true);
        otpRequestRepository.save(otpSaved);

        logger.info(otpCode);
        return otpCode;
    }

    public OTPRequestDto checkOtp(String otp) {
        OTPRequestDto otpRequestDto = null;
        try {
            OTPRequest otpRequest = otpRequestRepository.findByOtpCode(otp);
            if (otpRequest == null) {
                otpRequestDto = modelMapper.map(null, OTPRequestDto.class);
            }
        } catch (NullPointerException e){
            e.printStackTrace();
        }
        return otpRequestDto;
    }
}
