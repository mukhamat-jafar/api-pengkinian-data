package equity.id.Myelifejotform.retrofit.otp;

import lombok.Data;

@Data
public class OTPNotificationRequest<T> {
    private String username;
    private String signature;
    private String action;
    private T value;

    public OTPNotificationRequest(String username, String signature, String action, T value) {
        this.username = username;
        this.signature = signature;
        this.action = action;
        this.value = value;
    }
}
