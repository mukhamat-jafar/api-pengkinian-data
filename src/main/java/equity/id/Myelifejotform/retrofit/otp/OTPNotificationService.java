package equity.id.Myelifejotform.retrofit.otp;

import equity.id.Myelifejotform.model.OTPRequest;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

@Service
public class OTPNotificationService<T> {

    private static final String BASE_URL = "https://eli-sit-api.myequity.id/";

    private Retrofit retrofit;
    private OTPNotificationApi otpNotificationApi;

    @Value("${otp.username}")
    private String otpUsername;

    @Value("${otp.signature}")
    private String otpSignature;

    @Value("${otp.action}")
    private String otpAction;

    public OTPNotificationService() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        otpNotificationApi = retrofit.create(OTPNotificationApi.class);
    }

    public OTPNotificationResponse postOTPNotification(OTPRequest otpRequest, String getOTP) throws IOException {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDateTime localDateNow = LocalDateTime.now();

        Map<String, String> value = new HashMap();
        value.put("message_type", otpRequest.getOtpType());
        value.put("type_code", otpRequest.getTypeCode());
        value.put("variable_no_1", otpRequest.getCustomerCode());
        value.put("variable_name_1", otpRequest.getCustomerName());
        value.put("sent_date", dtf.format(localDateNow));
        value.put("variable_no_2", otpRequest.getPhoneNo());
        value.put("email_address", otpRequest.getEmail());
        value.put("gender_code", otpRequest.getGenderCode());

        value.put("notes", getOTP);


        OTPNotificationRequest request = new OTPNotificationRequest(
                otpUsername,
                otpSignature,
                otpAction,
                value
        );

        try {
            Call<OTPNotificationResponse> dataResponse = otpNotificationApi.sendOTP(request);
            OTPNotificationResponse response = dataResponse.execute().body();
            return response;

        }catch (IOException e){
            throw new IOException("Error Bro!", e);
        }
    }
}
