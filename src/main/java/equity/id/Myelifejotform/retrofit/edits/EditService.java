package equity.id.Myelifejotform.retrofit.edits;

import equity.id.Myelifejotform.model.ChangeAddress;
import equity.id.Myelifejotform.retrofit.otp.OTPNotificationRequest;
import equity.id.Myelifejotform.service.ChangeAddressService;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;


@Service
public class EditService<T> {

    private static final String BASE_URL = "https://hookb.in/";
//    private static final String BASE_URL = "http://sit-mw01.myequity.id:10027/api-edits/index.php/api/";
    private Retrofit retrofit;
    private EditApi editApi;


    @Autowired
    private ChangeAddressService changeAddressService;

    @Value("${edit.username}")
    private String editUsername;

    @Value("${edit.password}")
    private String editPassword;

    @Value("${edit.appCode}")
    private String editAppCode;

    @Value("${edit.action}")
    private String editAction;

    public EditService() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        editApi = retrofit.create(EditApi.class);
    }

    public EditResponse postChangeAddress(MultipartFile image, ChangeAddress newChangeAddress) throws IOException {

        EditRequest request = new EditRequest();
        request.setUsername(editUsername);
        request.setPassword(editPassword);
        request.setApp_code(editAppCode);
        request.setAction(editAction);

        JSONObject values = new JSONObject();
        values.put("eform_id", "69");
        values.put("policy_no", newChangeAddress.getPolicyNo());
        values.put("customer_name", newChangeAddress.getPolicyHolder());
        values.put("address_1", newChangeAddress.getAddress1());
        values.put("address_2", newChangeAddress.getAddress2());
        values.put("address_3", newChangeAddress.getAddress3());
        values.put("city", newChangeAddress.getStateCode());
        values.put("state", newChangeAddress.getStateCode());
        values.put("postal_code", newChangeAddress.getPostalCode());
        values.put("country_code", newChangeAddress.getCountryCode());
        values.put("fax_no", newChangeAddress.getFaxNo());
        values.put("phone_no", newChangeAddress.getPhoneNo());
        values.put("mobile_no", newChangeAddress.getHandphoneNo());
        values.put("email", newChangeAddress.getEmailAddress());
        values.put("documents", newChangeAddress.getEmailAddress());

        //Convert Image to Base64
        String encodedImage = Base64.getEncoder().encodeToString(image.getBytes());
        values.put("documents", encodedImage);

        request.setValues(values);
        try {
            Call<EditResponse> dataResponse = editApi.postToEdit(request);
            EditResponse response = dataResponse.execute().body();
            return response;

        }catch (IOException e){
            throw new IOException("Error Bro!", e);
        }
    }
}
